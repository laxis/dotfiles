# My Dotfiles  
  
this is set using a bare repository, since it's super simple once set up (shoutout to [StreakyCobra on HN](https://news.ycombinator.com/item?id=11071754))

## Install on a new machine

### Clone the files
```
git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:laxis/dotfiles.git $HOME/dotfiles-tmp 
cp ~/dotfiles-tmp/.gitmodules ~ # if there are any submodules
rm -r ~/dotfiles-tmp/
```

define an alias for easier management (just use `config` instead of `git` while in your home dir).
this is already added to the .zshrc
```
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```
and then run 
```
config config status.showUntrackedFiles no
```
to make sure no files are added by default

## Software
- alacritty / kitty / st (terminal)
- i3 (window manager)
- dunst (notifications)
- mycli (db cli)
- neovim
- oh-my-zsh
  - add private scripts to .oh-my-zsh/custom
- ripgrep
- fzf
- lnav
- htop / gotop
- rofi
- feh ?
- vscode (use settings sync)
- glances (monitoring)
- visidata (visualisation)

### Install Ripgrep
https://github.com/BurntSushi/ripgrep


### Theme
[nord](https://www.nordtheme.com/) everything
