call plug#begin('~/.local/share/nvim/plugged')
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
call plug#end()

filetype on
filetype plugin on
filetype indent on
syntax on
colorscheme nord

set autoindent
set autoread
set backspace=indent,eol,start
set clipboard+=unnamedplus
set colorcolumn=0
set cursorline
set encoding=utf-8
set expandtab
set foldmethod=indent
set hidden
set history=100
set hlsearch
set ignorecase 
set incsearch 
set laststatus=2
set lazyredraw
set mouse=a
set nobackup
set nocompatible
set nofoldenable
set noswapfile
set nowrap
set nowritebackup
set number
set relativenumber
set shiftwidth=4
set shortmess +=c
set showmatch
set signcolumn=yes
set smartcase 
set smartindent
set smarttab
set so=999
set splitbelow
set splitright
set tabstop=4
set termguicolors
set updatetime=300
set visualbell
set wildignore+=.hg,.git,.svn "Version Controls"
set wildignore+=*.aux,*.out,*.toc "Latex Indermediate files"
set wildignore+=*.DS_Store "OSX SHIT"
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg "Binary Imgs"
set wildignore+=*.luac "Lua byte code"
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest "Compiled Object files"
set wildignore+=*.orig "Merge resolution files"
set wildignore+=*.pyc "Python Object codes"
set wildignore+=*.spl "Compiled spelling world list"
set wildignore+=*.sw? "Vim swap files"
set wildignore+=node_modules
set wildmenu
set wildmode=list:longest:full,full


" git gutter
let g:gitgutter_grep = 'rg'
let g:gitgutter_enabled = 1

" goyo
let g:goyo_width = 120

" mappings
nnoremap <leader>s :source ~/.config/nvim/init.vim<CR>
nnoremap <leader>, :tabe ~/.config/nvim/init.vim<CR>
tnoremap <Esc> <C-\><C-n> " map Esc in terminal to behave like other modes
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<CR> " cancel search with escape
nnoremap <leader>z :Goyo<CR>

" Map file/buffer movement stuff
nnoremap <leader>e :Explore<CR>
nnoremap <leader>o :ls<CR> :b 
nnoremap <leader>p :FZF<CR>

" Map Ctrl-HJKL for split movement
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" map ctrl-arrows to resize
nnoremap <C-Left> :vertical resize +2<CR>
nnoremap <C-Up> :resize +2<CR>
nnoremap <C-Down> :resize -2<CR>
nnoremap <C-Right> :vertical resize -2<CR>


" Netrw Config
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_sort_sequence = '[\/]$,*' " sort with dirs on top

" Airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'jsformatter'

" CoC config
let g:coc_global_extensions = ['coc-tsserver', 'coc-prettier', 'coc-eslint', 'coc-docker', 'coc-emoji', 'coc-highlight', 'coc-lists']

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
nmap <silent> gf <Plug>(coc-format)
" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
"
" Declare command to be able to use :Find to search with rg
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
